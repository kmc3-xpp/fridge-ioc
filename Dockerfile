FROM python:latest

ARG PACKAGE_SRC=/package_src
ARG PACKAGE_FROM_PIP=yes
ARG PACKAGE_FLAVORS=test
ARG PACKAGE_NAME=ls336-ioc

RUN useradd -u 9999 cthulhu && mkdir /home/cthulhu && chown cthulhu /home/cthulhu

RUN if [ -d "$PACKAGE_SRC" ]; then \
        echo "Installing from local directory: $PACKAGE_SRC" ;\
        pip install $PACKAGE_SRC[$PACKAGE_FLAVORS] ;\
    elif [ "$PACKAGE_FROM_PIP" = "yes" ]; then \
        echo "Installing from pip" ;\
        pip install $PACKAGE_NAME[$PACKAGE_FLAVORS] ;\
    else \
        echo "\n  ***\n  *** Source not found at \$PACKAGE_SRC ($PACKAGE_SRC). \n  ***\n" ;\
    fi

USER cthulhu

CMD ls336-ioc
